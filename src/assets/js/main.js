import $ from 'jquery'
import 'jquery-mask-plugin/dist/jquery.mask'
import 'slick-carousel/slick/slick'
import wow from 'wowjs'

ymaps.ready(init);

let map = null

function init() {

    if (window.innerWidth > 992 && !map) {
        initMap()
    } else {
        window.addEventListener('resize', function () {
            if (window.innerWidth > 992 && !map) {
                initMap()
            }
        })
    }
}

function initMap() {
    map = new ymaps.Map("map", {
        center: [47.218394, 39.698511],
        zoom: 16
    });

    ymaps.modules.require(['Placemark'], function (Placemark) {
        map.geoObjects
            .add(new Placemark([47.218792, 39.702391], {}, {
                preset: 'islands#icon',
                iconColor: '#177ee6'
            }))
            .add(new Placemark([47.216209, 39.708949], {}, {
                preset: 'islands#icon',
                iconColor: '#177ee6'
            }))
    });

}

$(function () {

    new wow.WOW().init()

    $('.coaches__slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        infinite: false,
        fade: true,
        arrows: false,
        swipe: false,
        adaptiveHeight: true,
    })

    $('.coaches__arrow-next').on('click', function () {
        $('.coaches__slider').slick('slickNext')
    })

    $('.coaches__arrow-prev').on('click', function () {
        $('.coaches__slider').slick('slickPrev')
    })

    $('.coaches__more').on('click', function () {
        $(this).closest('.coaches__content').find('.coaches__content-body').addClass('full')
        $(this).addClass('hide')
        $('.coaches__slider').slick('setPosition')
    })


    document.addEventListener('change', checkboxChange)

    function checkboxChange(event) {
        if (event.target.type !== 'checkbox') return

        const target = event.target
        const field = target.closest('.checkbox').querySelector('.checkbox__field')

        if (target.checked) {
            field.classList.add('checked')
        } else {
            field.classList.remove('checked')
        }
    }


    document.addEventListener('click', btnModal)

    function btnModal(event) {
        if (!event.target.classList.contains('btn-modal')) return

        openModal('form-module')
    }

    function openModal(module, event) {
        const modal = document.querySelector('.modal')
        const moduleHTML = document.querySelector('.' + module).innerHTML

        modal.querySelector('.modal__body').innerHTML = moduleHTML

        $(modal).find('input[type="tel"]').each(function () {
            $(this).mask('+7 (000) 000 00 00', {
                onChange: function (cep, e) {
                    checkPhone(cep, e);
                }
            });
        });

        document.body.style.overflow = 'hidden'
        modal.style.display = 'flex'
        setTimeout(() => {
            modal.classList.add('show')
        }, 0)

        document.addEventListener('click', closeModal)
        document.addEventListener('keyup', closeModal)
    }

    function closeModal(event) {
        if ((event.target && event.target.classList.contains('modal')) || event.code === 'Escape') {
            const modal = document.querySelector('.modal')

            modal.classList.remove('show')
            document.body.style.overflow = ''

            setTimeout(() => {
                modal.style.display = ''
            }, 300)
        }
    }


    document.addEventListener('click', navigation)

    function navigation(event) {
        if (!event.target.closest('.navigation') || !event.target.closest('.navigation__link')) return

        event.preventDefault()

        const target = event.target.closest('.navigation__link').dataset.target
        if (!target) return

        let top = 0

        if (target !== 'main') {
            const el = document.querySelector('.' + target)
            top = $(el).offset().top
        }

        if (event.target.closest('.menu')) {
            $('.menu').removeClass('show')
            document.removeEventListener('click', closeMenu)
        }

        $('html, body').animate({
            scrollTop: top
        }, 500);
    }


    document.addEventListener('change', function () {
        checkInput(event.target);
    });

    document.addEventListener('submit', checkForm);

    let errs = {};
    document.querySelectorAll('form').forEach((form) => {
        errs[form.getAttribute('name')] = [];
    });

    $('input[type="tel"]').each(function () {
        $(this).mask('+7 (000) 000 00 00', {
            onChange: function (cep, e) {
                checkPhone(cep, e);
            }
        });
    });

    function checkInput(target) {
        let type = target.type;
        let form = target.closest('form');
        const formName = form.getAttribute('name')
        let regexp;
        let index = errs[formName].indexOf(target);

        switch (type) {
            case 'text':
                if (target.dataset.onlywords) regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
                else regexp = /^.+$/

                if (!target.value) {
                    form[target.name + 'Valid'] = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.remove('success');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else if (regexp.test(target.value)) {
                    form[target.name + 'Valid'] = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.add('success');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else {
                    form[target.name + 'Valid'] = false;
                    target.closest('.input-wrapper').classList.remove('success');
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                }
                break;
            case 'email':
                regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;

                if (!target.value) {
                    form.emailValid = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.remove('success');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else if (regexp.test(target.value)) {
                    form.emailValid = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.add('success');
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }
                } else {
                    form.emailValid = false;
                    target.closest('.input-wrapper').classList.remove('success');
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                }
                break;
            case 'checkbox':
                if (target.checked) {
                    if (index != -1) {
                        errs[formName].splice(index, 1);
                    }

                    target.closest('.checkbox').querySelector('.checkbox__field').classList.remove('error');
                } else {
                    if (!errs[formName].includes(target)) {
                        errs[formName].push(target);
                    }
                    target.closest('.checkbox').querySelector('.checkbox__field').classList.add('error');
                }
                break;
        }
    }

    function checkPhone(value, event) {
        let target = event.target;
        let form = target.closest('form');
        const formName = form.getAttribute('name')
        let index = errs[formName].indexOf(target);

        if (target.value.length == 0) {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('error');
            target.closest('.input-wrapper').classList.remove('success');
            if (index != -1) {
                errs[formName].splice(index, 1);
            }
        } else if (target.value.length == 18) {
            form.phoneValid = true;
            target.closest('.input-wrapper').classList.remove('error');
            target.closest('.input-wrapper').classList.add('success');
            if (index != -1) {
                errs[formName].splice(index, 1);
            }
        } else {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('success');
            target.closest('.input-wrapper').classList.add('error');
            if (!errs[formName].includes(target)) {
                errs[formName].push(target);
            }
        }
    }

    function checkForm() {
        event.preventDefault();

        let form = event.target;
        const formName = form.getAttribute('name')
        errs[formName] = []

        let data = new FormData();
        if (form.name) {
            if (!(form.nameValid) && !(errs[formName].includes(form.name))) {
                errs[formName].push(form.name);
            } else {
                data.append('name', form.name.value);
            }
        }
        if (form.address) {
            if (!(form.addressValid) && !(errs[formName].includes(form.address))) {
                errs[formName].push(form.address);
            } else {
                data.append('address', form.address.value);
            }
        }
        if (form.phone) {
            if (!(form.phoneValid) && !(errs[formName].includes(form.phone))) {
                errs[formName].push(form.phone);
            } else {
                data.append('phone', form.phone.value);
            }
        }
        if (form.email) {
            if (!(form.emailValid) && !(errs[formName].includes(form.email))) {
                errs[formName].push(form.email);
            } else {
                data.append('email', form.email.value);
            }
        }

        if (form.confirm && !(form.confirm.checked) && !(errs[formName].includes(form.confirm))) {
            errs[formName].push(form.confirm);
            form.confirm.closest('.checkbox').querySelector('.checkbox__field').classList.add('error');
        }

        if (errs[formName].length != 0) {
            for (let err of errs[formName]) {
                if (err.name != 'confirm') {
                    err.closest('.input-wrapper').classList.remove('success');
                    err.closest('.input-wrapper').classList.add('error');
                }
            }
            return;
        }

        if (form.closest('.modal')) {
            let modalBody = form.closest('.modal__body');
            modalBody.classList.add('off');

            setTimeout(() => {
                modalBody.innerHTML = document.querySelector('.thanks-module').innerHTML;
                setTimeout(() => {
                    modalBody.classList.remove('off');
                }, 200);
            }, 300);
        } else {
            openModal('thanks-module');
            clearForm(form);
        }

    }

    function clearForm(form) {
        if (form.name) {
            form.name.value = '';
            form.name.closest('.input-wrapper').classList.remove('success');
            form.nameValid = false;
        }
        if (form.email) {
            form.email.value = '';
            form.email.closest('.input-wrapper').classList.remove('success');
            form.emailValid = false;
        }
        if (form.phone) {
            form.phone.value = '';
            form.phone.closest('.input-wrapper').classList.remove('success');
            form.phoneValid = false;
        }
        if (form.address) {
            form.address.value = '';
            form.address.closest('.input-wrapper').classList.remove('success');
            form.addressValid = false;
        }
    }


    $('.menu-btn').on('click', function (event) {
        if ($('.menu').hasClass('show')) {
            $('.menu').removeClass('show')
            document.removeEventListener('click', closeMenu)
        } else {
            $('.menu').addClass('show')
            document.addEventListener('click', closeMenu)
        }
    })

    $('.menu').find('.menu-close').on('click', function () {
        $('.menu').removeClass('show')
        document.removeEventListener('click', closeMenu)
    })

    function closeMenu(event) {
        if (event.target.closest('.menu') || event.target.closest('.menu-btn')) return

        $('.menu').removeClass('show')
        document.removeEventListener('click', closeMenu)
    }


    $('.mobile-btn').on('click', function (event) {
        if ($('.mobile-menu').hasClass('show')) {
            $('.mobile-menu').removeClass('show')
            document.removeEventListener('click', closeMobileMenu)
        } else {
            $('.mobile-menu').addClass('show')
            document.addEventListener('click', closeMobileMenu)
        }
    })

    $('.mobile-menu').find('.menu-close').on('click', function () {
        $('.mobile-menu').removeClass('show')
        document.removeEventListener('click', closeMobileMenu)
    })

    function closeMobileMenu(event) {
        if (event.target.closest('.mobile-menu') || event.target.closest('.mobile-btn')) return

        $('.mobile-menu').removeClass('show')
        document.removeEventListener('click', closeMobileMenu)
    }


    $('.map__close').on('click', function () {
        $('.map__content').addClass('hide')
    })

    $('.map__open').on('click', function () {
        $('.map__content').removeClass('hide')
    })

    $('.map__address-item').on('click', function () {
        const coords = $(this).data('coords')

        map.setCenter(coords.split(','))
    })

    $('.map__order-btn').on('click', function () {
        $('.map__order-list').slideDown(200)

        setTimeout(() => {
            document.addEventListener('click', closeOrderList)
        }, 0)
    })

    function closeOrderList(event) {
        if (event.target.closest('.map__order-list')) return

        $('.map__order-list').slideUp(300)
        document.removeEventListener('click', closeOrderList)
    }

})


